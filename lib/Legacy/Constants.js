'use strict';

// Ayana Gateway epoch (2017-01-01T00:00:00.000Z)
exports.AyanaEpoch = 1483228800000;

/**
 * Ayana Gateway OPCodes
 */
exports.GWOPCodes = {
 DISPATCH: 0, // Events
 HEARTBEAT: 1, // Heartbeat
 IDENTIFY: 2, // Idenify Payload (client->server)
 LOG: 3, // Logs (client->server)
 HELLO: 4, // Gateway Hello (server->client)
 HBACK: 5, // Gateway HBACK (server->client)
 CONNECTED: 6, // Gateway Connected (server->client)
};

/**
 * Ayana Gateway DisconnectCodes
 */
exports.GWDisconnectCodes = {
 TIMEOUT: 30001, // Client took to long to idenfiy
 INVALID_IDENIFY: 30002, // Invalid idenify payload was given
 INVALID_TYPE: 30003, // Unmanaged client type in idenify payload
 HB_TIMEOUT: 30004, // Client failed to send enough heartbeats to keep connection alive (see heartbeatInterval in hello payload)

 // SHARDGROUP DC CODES
 SHARD_TOO_MANY: 30005, // Too many shards already exist on this connect, client should die or seek another master.
};

/**
 * Ayana Gateway StatusCodes
 */
exports.GWStatusCodes = {
 NO_ROUTE: 0, // Gateway can't route this pkt
 NO_CLIENTS: 1, // No Clients around to recieve the pkt 
};

/**
 * Ayana Gateway Route Methods
 */
exports.RouteMethods = {
 BROADCAST: 0, // Broadcast to all clients in a group
 EXLUSIVE: 1, // Don't send message unless a single client was resolved
 ONERANDOM: 2, // Send to a single random client in a group.
};

/**
 * Ayana PRO subscription service response codes
 */
exports.SubscriptionServiceCodes = {
 OK: 0,
 INVALID_REQUEST: 1,
 UNEXPECTED_ERROR: 2,
 NO_SUBSCRIPTIONS: 3,
 GUILD_ALREADY_UPGRADED: 4,
 NO_MORE_UPGRADES_AVAILABLE: 5,
 GUILD_NOT_UPGRADED: 6,
 NO_USER_DATA: 7,
 TOO_MANY_INSTANCES: 8,
 SUBSCRIPTION_NOT_FOUND: 9,
 NO_PATREON: 10,
 PLEDGE_TOO_LOW: 11,
 DONATOR_TEXT_NOT_SET: 12,
};

/**
 * Ayana PRO subscriptions service redeem system codes
 */
exports.RedeemCodes = {
 SUCCESS: 0,
 INVALID_CODE: 1,
 CODE_ALREADY_USED: 2,
 CODE_NOT_FOUND: 3,
 REDEEM_ERROR: 4,
 RELOAD_ERROR: 5,
 AWAIT_PUNISHMENT: 6,
};

/**
 * Event service response codes
 */
exports.EventServiceCodes = {
 OK: 0,
 INVALID_REQUEST: 1,
 UNEXPECTED_ERROR: 2,
 EVENT_NOT_FOUND: 3,
 EVENT_ADD_FAILURE: 4,
 EVENT_REMOVE_FAILURE: 5,
 EVENT_HANDLER_NOT_RUNNING: 6,
 WRONG_EVENT_SHARD: 7,
}