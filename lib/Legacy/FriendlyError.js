'use strict';

/**
 * An error that can be displayed to the user.
 * @extends {Error}
 */

class FriendlyError extends Error {
 /**
  * @param {string} msg - The Error Message
  */
 constructor(msg) {
  super(msg);

  this.name = 'FriendlyError';
 }

 static isFriendlyError(arg) {
  return arg instanceof FriendlyError;
 }
}

module.exports = FriendlyError;
