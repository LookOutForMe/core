'use strict';

// Initally pulled from https://github.com/devsnek/node-snowfall/blob/924c22f2e090cce3aac0bc72748d1a8b4ca5fbf6/src/index.js

const { AyanaEpoch } = require('./Constants');

const Long = require('long');

function pad(v, n, c = '0') {
  return String(v).length >= n ? String(v) : (String(c).repeat(n) + v).slice(-n);
}

const COMMON_EPOCHS = {
  AYANA: AyanaEpoch,
  UNIX: 0,
  TWITTER: 1288834974657,
  DISCORD: 1420070400000,
};

/**
  * A Snowflake, based off of the twitter snowflake
  * ```
  * If we have a snowflake '266241948824764416' we can represent it as binary:
  *
  * 64                                          22     17     12          0
  *  000000111011000111100001101001000101000000  00001  00000  000000000000
  *   ms since epoch (discord in this example)   worker  pid     interval
  * ```
  * Note: this generator hardcodes the worker id as 1 and the process id as 0
  * @typedef {string} Snowflake
  */
class Flurry {
  constructor({ workerID, processID, interval, epoch } = {}) {
    this.workerID = workerID || 0;
    this.processID = processID || 0;
    this.interval = interval || 0;
    this.epoch = epoch || COMMON_EPOCHS.AYANA;
  }

  /**
  * Generate a Snowflake
  * @returns {Snowflake} The generated Snowflake
  */
  next({ date, interval } = {}) {
    if (this.interval >= 4096) this.interval = 0;
    const TIMESTAMP = pad(((date || Date.now()) - this.epoch).toString(2), 42);
    const WORKER = pad(this.workerID.toString(2), 5);
    const PROCESS = pad(this.processID.toString(2), 5);
    const INTERVAL = pad((typeof interval === 'number' ? interval : this.interval++).toString(2), 12);
    const BINARY = `${TIMESTAMP}${WORKER}${PROCESS}${INTERVAL}`;
    return Long.fromString(BINARY, 2).toString();
  }

  /**
   * Deconstruct a Snowflake
   * @param {Snowflake} snowflake Snowflake to deconstruct
   * @param {number} [epoch=this.epoch] Epoch used to calculate the date
   * @returns {DeconstructedSnowflake} Deconstructed snowflake
   */
  deconstruct(snowflake, epoch) {
    epoch = typeof epoch === 'number' ? epoch : this.epoch;
    if (!epoch) epoch = 0;
    const BINARY = pad(Long.fromString(snowflake).toString(2), 64);
    return {
      date: new Date(parseInt(BINARY.substring(0, 42), 2) + epoch),
      workerID: parseInt(BINARY.substring(42, 47), 2),
      processID: parseInt(BINARY.substring(47, 52), 2),
      interval: parseInt(BINARY.substring(52, 64), 2),
      binary: BINARY,
    };
  }

  /**
   * Deconstruct a Snowflake
   * @param {Snowflake} snowflake Snowflake to deconstruct
   * @param {number} [epoch=0] Epoch used to calculate the date
   * @returns {DeconstructedSnowflake} Deconstructed snowflake
   */
  static deconstruct(snowflake, epoch) {
    return this.prototype.deconstruct(snowflake, epoch);
  }
}

Flurry.EPOCHS = COMMON_EPOCHS;

module.exports = Flurry;