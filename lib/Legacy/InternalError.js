'use strict';

const FriendlyError = require('./FriendlyError');

/**
 * Error containing a type. Useful for being caught and checking type
 */
class InternalError extends FriendlyError {
 /**
  * @param {string} type - Programming friendly error type
  * @param {string} msg - User friendly error text
  */
 constructor(type, msg = null) {
  super(msg ? msg : type);

  this.name = 'InternalError';

  this._type = type;
 }

 get type() {
  return this._type;
 }

 static isInternalError(arg) {
  return arg instanceof InternalError;
 }
}

module.exports = InternalError;
