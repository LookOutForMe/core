'use strict';

const GatewayClient = require('./GatewayClient');

const Collection2 = require('../Collection2');

const { EventEmitter } = require('events');


class MultiGatewayClient extends EventEmitter {
 constructor({ type }) {
  super();
  this.setMaxListeners(0);

  if (!type) throw new Error('No type specified.');

  this.type = type;

  this._clients = new Collection2();
 }

 connect({ dsns }) {
  if (!dsns) throw new Error('Missing DSNs');
  if (!Array.isArray(dsns)) throw new Error('Expected DSNs to be array.');

  for (const dsn of dsns) {
   try {
    const cli = new GatewayClient({ type: this.type, dsn });

    cli.on('open', this.onOpen.bind(this, cli));
    cli.on('connected', this.onConnected.bind(this, cli));
    cli.on('dispatch', this.onDispatch.bind(this, cli));
    cli.on('error', this.onError.bind(this, cli));
    cli.on('close', this.onClose.bind(this, cli));
    cli.on('rawWS', this.onRawWS.bind(this, cli));

    cli.connect();
   } catch (e) {
    throw e;
   }
  }
 }

 onOpen(cli, e) {
  this.emit('open', {
   host: e.ip, port: e.port,
  });
 }

 onConnected(cli) {
  if (!cli.GWID) return;
  this._clients.set(cli.GWID, cli);

  this.emit('connected', {
   GWID: cli.GWID,
   host: cli.ip, port: cli.port,
  });
 }

 onDispatch(cli, pkt) {
  if (!cli.GWID) return;

  this.emit('dispatch', {
   GWID: cli.GWID,
   host: cli.ip, port: cli.port,
   pkt,
  });
 }

 onError(cli, e) {
  this.emit('error', {
   GWID: cli.GWID,
   host: cli.ip, port: cli.port,
   e,
  });
 }

 onClose(cli) {
  if (!cli.GWID) return;

  this._clients.delete(cli.GWID);

  this.emit('close', {
   GWID: cli.GWID,
   host: cli.ip, port: cli.port,
  });
 }

 onRawWS(cli, pkt) {
  if (cli.GWID) return;

  this.emit('rawWS', {
   GWID: cli.GWID,
   host: cli.ip, port: cli.port,
   pkt,
  });
 }

 getClient(GWID) {
  if (!this._clients.has(GWID)) return null;

  return this._clients.get(GWID);
 }

 multiSendPKT(ob, GWID) {
  let clients = this._clients.values();
  if (GWID) {
   clients = [];

   for (const [id, cli] of this._clients.entries()) {
    if (id === GWID) clients.push(cli);
   }

   if (clients.length < 1) throw new Error('Unable to find client');
  }

  for (const cli of clients) cli.sendPKT(ob);
 }

 sendEventReply(GWID, ...args) {
  if (!GWID) throw new Error('GWID required');
  if (!this._clients.has(GWID)) throw new Error('Unknown GWID');

  return this._clients.get(GWID).sendEventReply(...args);
 }

 sendPKTForReply(GWID, ...args) {
  if (!GWID) throw new Error('GWID required');
  if (!this._clients.has(GWID)) throw new Error('Unknown GWID');

  return this._clients.get(GWID).sendPKTForReply(...args);
 }
}

module.exports = MultiGatewayClient;
