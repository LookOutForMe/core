'use strict';

const InternalError = require('../InternalError');

const { GWOPCodes } = require('../Constants');

const Flurry = require('../Flurry');

const WS = require('uws');
const { EventEmitter } = require('events');

const url = require('url');

class GatewayClient extends EventEmitter {
 constructor({ type, port, ip, dsn }) {
  super();
  this.setMaxListeners(0);

  this.GWID = null;
  this.GWVersion = null;

  this.type = type || '_default';
  this.port = port || 27532;
  this.ip = ip || '127.0.0.1';

  if (dsn) {
   const parsed = url.parse(dsn, true);

   if (!parsed.hostname || !parsed.port) throw new Error('Invalid DSN: Missing Hostname or port');

   this.ip = parsed.hostname;
   this.port = parsed.port;
  }

  this.cli = null;
  this.connected = false;
  this.ready = false;
  this.handshaking = false;

  this.autoReconnect = true;
  this.heartbeat = -1;

  this.flurry = new Flurry();
  this.reconnect = -1;

  // Tracking gateway latecy
  this._latency = [null, null, null, null, null];
 }

 /**
  * Get average gateway ping latency, calculated based on pings
  */
 get ping() {
  // get non null entries
  const clean = this._latency.filter(l => !!l);

  return clean.reduce((a, b) => a + b, 0) / clean.length;
 }


 /**
  * returns the array containing latency messurements
  */
 get latencyData() {
  return this._latency.filter(l => !!l);
 }

 connect() {
  if (this.cli) this.cli.removeAllListeners();
  this.cli = new WS(`wss://${this.ip}:${this.port}`, {
   rejectUnauthorized: false,
  });

  this.cli.on('open', this.onConnection.bind(this));
  this.cli.on('message', this.onMessage.bind(this));
  this.cli.on('error', this.onError.bind(this));
  this.cli.on('close', this.onClose.bind(this));
 }

 disconnect() {
  this.autoReconnect = false;
  this.close(0, 'Closed manually');
 }

 onConnection() {
  this.connected = true;
  this.ready = false;
  this.handshaking = true;
  this.emit('open', { port: this.port, ip: this.ip });
 }

 onMessage(d) {
  let pkt = null;
  try {
   pkt = JSON.parse(d.toString());
  } catch (e) {
   return;
  }

  this.emit('rawWS', { dir: 'in', pkt });

  switch (pkt.op) {
   case GWOPCodes.DISPATCH:
    this.emit(`dispatch`, pkt);
    break;

   case GWOPCodes.HELLO:
    this.ready = false;
    this.handshaking = true;
    this.emit('handshaking');

    if (pkt.d.v) {
     this.GWID = pkt.d.id;
     this.GWVersion = pkt.d.v;
    }

    const hello = this.onHello(pkt);
    hello.t = this.type;

    this.sendPKT({
     op: GWOPCodes.IDENTIFY,
     d: hello,
    });

    this.sendHeartbeat();
    if (pkt.d.heartbeatInterval) this.startHeartbeat(pkt.d.heartbeatInterval);
    break;

   case GWOPCodes.CONNECTED:
    this.ready = true;
    this.handshaking = false;
    this.emit('connected');

    // just an alias
    this.emit('ready');
    break;

   case GWOPCodes.HBACK:
    this.emit(`hback`);
    break;
  }
 }

 /**
  * Object to be sent with idenify payload
  * @override
  */
 onHello(pkt) {
  return {};
 }

 onError(e) {
  this.stopHeartbeat();
  this.connected = false;
  this.emit('error', e);
  if (this.reconnect == -1 && this.autoReconnect) {
   this.reconnect = setTimeout(() => {
    this.reconnect = -1;
    this.connect();
   }, 5 * 1000);
  }
 }

 onClose() {
  this.stopHeartbeat();
  this.connected = false;
  this.emit('close', null);
  if (this.reconnect == -1 && this.autoReconnect) {
   this.reconnect = setTimeout(() => {
    this.reconnect = -1;
    this.connect();
   }, 5 * 1000);
  }
 }

 send(raw) {
  return this.cli.send(raw);
 }

 sendJSON(ob) {
  if (!this.ready && ob.op !== undefined && ob.op === 0) throw new InternalError('gwcli.notReady', 'Dispatch Packet sent prior to client being ready.');

  this.emit('rawWS', { dir: 'out', pkt: ob });
  return this.send(JSON.stringify(ob));
 }

 sendPKT(...args) {
  return this.sendJSON(...args);
 }

 // eslint-disable-next-line
 /**
  * Accepts a partial dispatch packet, appends opcode and event
  * @param {string} t - Name of the event we are sending (pkt.t)
  * @param {string} to - What ClientGroup to route this packet to (pkt.dst.t)
  * @param {object} raw - Raw packet to be formatted with dst and dispatch opcode
  * @param {boolean} gw - Should this go to directly to gw (pkt.dst.gw) omit if not needed
  */
 sendEvent(t, to, raw, gw) {
  if (!t || !to) return false;
  raw = raw || {};

  // append dispatch opcode and event name
  raw.op = GWOPCodes.DISPATCH;
  raw.t = t;

  // set d to null if nothing was specified
  if (typeof raw.d == 'undefined') raw.d = null;

  // append dst routing
  if (!raw.dst) raw.dst = {};
  raw.dst.t = to;

  if (gw) raw.dst.gw = true;

  return this.sendPKT(raw);
 }

 // eslint-disable-next-line
 /**
  * Send a packet to gateway and wait for a reply
  * @param {object} pkt - The packet to be send
  * @param {int} timeout - How long to wait for a reply (seconds)
  */
 sendPKTForReply(pkt, timeout) {
  return new Promise((resolve, reject) => {
   timeout = timeout || 3;
   const nonce = this.flurry.next();

   if (!pkt.dst) pkt.dst = {};
   pkt.dst.nonce = nonce;

   this.sendPKT(pkt);

   const handleReply = rpkt => {
    if (!rpkt.src || !rpkt.src.nonce) return;
    if (rpkt.src.nonce != nonce) return;
    this.removeListener(`dispatch`, handleReply); clearTimeout(timer);

    resolve(rpkt);
   };

   this.on('dispatch', handleReply);

   const timer = setTimeout(() => {
    reject(new Error('TIMEOUT'));
   }, timeout * 1000);
  });
 }

 // eslint-disable-next-line
 /**
  * Accepts a partial dispatch packet, appends opcode and event and waits for reply
  * @param {string} t - Name of the event we are sending (pkt.t)
  * @param {string} to - What ClientGroup to route this packet to (pkt.dst.t)
  * @param {object} raw - Raw packet to be formatted with dst and dispatch opcode
  * @param {boolean} gw - Should this go to directly to gw (pkt.dst.gw) omit if not needed
  * @param {int} timeout - How long to wait for a reply (seconds)
  */
 sendEventForReply(t, to, raw, gw, timeout) {
  return new Promise((resolve, reject) => {
   if (!t || !to) return reject();
   raw = raw || {};

   // append dispatch opcode and event name
   raw.op = GWOPCodes.DISPATCH;
   raw.t = t;

   // set d to null if nothing was specified
   if (typeof raw.d == 'undefined') raw.d = null;

   // append dst routing
   if (!raw.dst) raw.dst = {};
   raw.dst.t = to;

   if (gw) raw.dst.gw = true;

   return this.sendPKTForReply(raw, timeout)
   .then(resolve).catch(reject);
  });
 }

 // eslint-disable-next-line
 /**
  * Correctly formats a reply packet if the recPKT contains a src.nonce
  * @param {object} pkt - the packet to send
  * @param {object} recPKT - the packet that we are replying to
  */
 sendReply(pkt, recPKT) {
  if (!pkt || typeof pkt !== 'object') return false;
  if (!recPKT) return this.sendPKT(pkt);

  // check if recPKT.src.nonce is a thing
  if (!recPKT.src || !recPKT.src.nonce) return this.sendPKT(pkt);

  if (!pkt.dst) {
   pkt.dst = {};

   // if no dst was defined then we deliever it to recPKT.src._id
   pkt.dst._id = recPKT.src._id;
  }

  // copy src.nonce to dst.nonce
  pkt.dst.nonce = recPKT.src.nonce;

  return this.sendPKT(pkt);
 }

 // eslint-disable-next-line
 /**
  * Accepts a partial dispatch packet, appends opcode, event and recPKT.src.nonce if applicable
  * @param {string} t - Name of the event we are sending (pkt.t)
  * @param {string} to - What ClientGroup to route this packet to (pkt.dst.t)
  * @param {object} raw - Raw packet to be formatted with dst and dispatch opcode
  * @param {object} recPKT - The packet we are replying to
  * @param {boolean} gw - Should this go to directly to gw (pkt.dst.gw) omit if not needed
  */
 sendEventReply(t, to, raw, recPKT, gw) {
  if (!t || !to) return false;
  raw = raw || {};

  // append dispatch opcode and event name
  raw.op = GWOPCodes.DISPATCH;
  raw.t = t;

  // set d to null if nothing was specified
  if (typeof raw.d == 'undefined') raw.d = null;

  // append dst routing
  if (!raw.dst) raw.dst = {};
  raw.dst.t = to;

  if (gw) raw.dst.gw = true;

  return this.sendReply(raw, recPKT);
 }

 close(code, reason) {
  this.stopHeartbeat();
  this.connected = false;
  // this.autoReconnect = false;

  this.cli.terminate();
  this.emit('close', { code, reason });
 }

 startHeartbeat(ms) {
  ms = parseInt(ms) > 1000 ? parseInt(ms) : 1000;
  this.heartbeat = setInterval(() => {
   this.sendHeartbeat();
  }, ms);
 }

 stopHeartbeat() {
  clearInterval(this.heartbeat);
 }

 sendHeartbeat() {
  if (!this.cli || !this.connected) return;

  this.emit('heartbeat', null);


  const handleLatency = () => {
   // pop off the last elem
   this._latency.pop();

   // push new latency to the top
   const latencyEnd = process.hrtime(latencyStart);
   const nanoseconds = (latencyEnd[0] * 1e9) + latencyEnd[1];
   const miliseconds = nanoseconds / 1e6;
   this._latency.unshift(miliseconds);
  };

  // listen for hback
  let handleReplay = () => {
   handleLatency();
   this.removeListener(`hback`, handleReplay);
   clearTimeout(timer);
  };

  let timer = setTimeout(() => {
   handleLatency();
   this.removeListener(`hback`, handleReplay);
   this.close(`HBACK Timeout.`);
  }, 5 * 1000);

  this.once(`hback`, handleReplay);

  // start 
  const latencyStart = process.hrtime();
  this.sendPKT({
   op: GWOPCodes.HEARTBEAT,
   d: null,
  });
 }
}

module.exports = GatewayClient;
