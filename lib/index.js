'use strict';

const { version } = require('../package.json');
exports.version = version || 'error';

exports.Generator = require('./Generator');

exports.Config = require('./Config');
exports.Logger = require('./Logger');
exports.ModuleLoader = require('./Loader');

exports.RedisClient = require('./Redis');
exports.Database = require('./Database');

exports.Sentry = require('./Sentry');

exports.EmitterTools = require('./EmitterTools');

exports.Util = require('./Util');

exports.Errors = require('./Errors');

exports.Legacy = {
	Net: require('./Legacy/Net'),
	Constants: require('./Legacy/Constants'),
};
