'use strict';

const fs = require('fs');
const path = require('path');

const Logger = require('../Logger');
const { asyncAwaitForEach } = require('../Util/Async');

/**
 * Loader class for loading and unloading modules
 * Module Events:
 *   onMount - Called as modules are being mounted
 *   onEnable - Called after all modules(core) are finished mounting, (Note: this event is only available to core modules)
 *   onUnmount - Called as module is being unmounted
 */
class ModuleLoader {
	/**
	 * Creates a new ModuleLoader instance
	 *
	 * @param {Object} opts 
	 * @param {Object} opts.base 
	 * @param {Object} opts.logger 
	 */
	constructor(opts) {
		opts = Object.assign({}, {
			base: {},
			logger: new Logger(),
		}, opts);

		this.logger = opts.logger;

		// base object
		this.base = opts.base;

		// core modules
		this.core = [];

		// normal modules
		this.modules = new Map();
	}

	async init(opts) {
		opts = Object.assign({}, {
			core: null,
			modules: null,
		}, opts);

		if (!opts.core) throw new Error(`'opts.core' is required`);

		await this.loadCoreModules(opts.core);
		if (opts.modules) await this.loadModules(opts.modules);
	}

	async loadCoreModules(location) {
		this.logger.info('Loader', `Loading Core Modules...`);
		const mods = await this.requireModules('core', location);

		// Check dependencies and attach module
		const determineMissing = dependencies => {
			return dependencies.reduce((a, c) => {
				if (!this.base.hasOwnProperty(c)) a.push(c);
				return a;
			}, []);
		};

		// handle modules waiting on dependencies
		let waiting = {};
		const checkWaiting = async() => {
			if (Object.keys(waiting).length < 1) return;

			const check = waiting;
			let newModuleLoaded = false;
			await asyncAwaitForEach(check, async({ mod, missing }, name) => {
				missing = determineMissing(missing);

				if (missing.length < 1) {
					// module dependencies resolved, delete from waiting
					delete check[name];

					await loadModule(mod);
					newModuleLoaded = true;
				} else {
					check[name] = {
						mod,
						missing,
					};
				}
			});

			waiting = check;
			if (newModuleLoaded) await checkWaiting();
		};

		// module dependencies have been met, ready to load
		let loadedModules = 0;
		const loadModule = async mod => {
			const name = mod.name;
			this.base[name] = mod.mod;

			loadedModules++;
			if (typeof this.base[name].onMount !== 'undefined') {
				try {
					await this.base[name].onMount();
				} catch (e) {
					loadedModules--;
					this.logger.error('Loader', `onMount: ${mod.name}(${mod.location}). ${e}`);
					if (mod.required) {
						this.logger.info('Loader', `'${mod.name}' has stated it is a required core module. Halting Execution...`);
						process.exit(1);
					}
				}
			}

			this.core.push(name);

			this.logger.debug('Loader', `Mounted Core Module ${mod.name}(${mod.location})`);
		};

		return asyncAwaitForEach(mods, async mod => {
			// if module has no dependencies, just load it
			if (!mod.dependencies || (mod.dependencies && mod.dependencies.length < 1)) {
				await loadModule(mod);
				await checkWaiting();
			} else {
				const name = mod.name;
				const missing = determineMissing(mod.dependencies);

				if (missing.length > 0) {
					this.logger.debug('Loader', `Waiting to mount core module ${mod.name}(${mod.location}). Missing dependencies: ${missing.join(', ')}`);
					waiting[name] = {
						mod,
						missing,
					};
				} else {
					await loadModule(mod);
					await checkWaiting();
				}
			}
		}).then(() => {
			if (Object.keys(waiting).length > 0) {
				Object.entries(waiting).forEach(([name, { mod, missing }]) => {
					this.logger.error('Loader', `Unable to mount core module ${mod.name}(${mod.location}). Unresolved dependencies: ${missing.join(', ')}`);
				});
			}

			this.core.forEach(async modName => {
				if (typeof this.base[modName].onEnable !== 'undefined') {
					try {
						await this.base[modName].onEnable();
					} catch (e) {
						this.logger.error('Loader', `onEnable: ${modName}. ${e}`);
					}
				}
			});

			this.logger.info('Loader', `Loaded and Mounted ${loadedModules} core modules.`);
		});
	}

	async loadModules(location) {
		this.logger.info('Loader', `Loading Modules...`);
		const mods = await this.requireModules('normal', location);

		// build api object (atm just consists of all core modules)
		const api = { logger: this.logger };
		this.core.forEach(m => {
			api[m] = this.base[m];
		});

		let loadedModules = 0;
		return asyncAwaitForEach(mods, async mod => {
			// instantiate module
			mod.mod = new mod.mod(this.base, api);

			loadedModules++;
			if (typeof mod.mod.onMount !== 'undefined') {
				try {
					await mod.mod.onMount();
				} catch (e) {
					loadedModules--;
					this.logger.error('Loader', `onMount: ${mod.name}(${mod.location}). ${e}`);
				}
			}

			this.modules.set(mod.name, mod);

			this.logger.debug('Loader', `Mounted Module ${mod.name}(${mod.location})`);
		}).then(() => {
			this.logger.info('Loader', `Loaded and Mounted ${loadedModules} modules.`);
		});
	}

	/**
	 * Loads and instantiates module files and directories of a specific module type
	 *
	 * @typedef {Object} ModuleData
	 * @property {string} name The name of the module
	 * @property {string} type The type of the module ('file' or 'directory')
	 * @property {string} location The file location of the module
	 * @property {Object} mod The instantiated module
	 *
	 * @param {string} moduleType The module type that should be loaded (currently 'core' or 'modules')
	 * @param {string} location filesystem location of modules to load
	 * @returns {[ModuleData]} The data of the loaded modules
	 */
	async requireModules(moduleType, location) {
		// List directory
		const files = await new Promise((resolve, reject) => {
			fs.readdir(location, (e, rawFiles) => {
				if (e) reject(`Failed to load modules: ${e}`);

				resolve(rawFiles.map(file => path.resolve(location, file)));
			});
		});

		const loaded = [];
		for (const file of files) {
			// Load file stats for knowing if it's a file or a directory
			const fileStats = await new Promise(resolve => {
				fs.lstat(file, (e, stats) => {
					if (e) return resolve(null);
					resolve(stats);
				});
			});

			// Check if those exist, if not we just error out
			if (!fileStats) {
				this.logger.warn('Loader', `Unable to load file/directory '${file}' (File stats failed)`);
				continue;
			}

			// Create module data object
			const data = {
				name: null,
				type: fileStats.isFile() ? 'file' : 'directory',
				location: file,
				mod: null,
				dependencies: null,
				required: null,
			};

			// Check for file and directory
			if (fileStats.isFile()) {
				// Validate it's a JavaScript file
				if (!data.location.toLowerCase().endsWith('.js')) {
					this.logger.warn('Loader', `Unable to load ${data.type} '${data.location}'. Not a Javascript file`);
					continue;
				}

				// Set name
				data.name = data.location.split('/').slice(-1)[0].slice(0, -3);
			} else {
				// Validate the directory contains an index.js
				if (!fs.existsSync(`${data.location}/index.js`)) {
					this.logger.warn('Loader', `Unable to load ${data.type} '${data.location}'. No index.js found`);
					continue;
				}

				// Set name
				data.name = data.location.split('/').slice(-1)[0];
			}

			this.logger.debug('Loader', `Loading module ${data.type} '${data.location}'`);

			// Require the module
			let Mod;
			try {
				// Delete from require cache
				delete require.cache[require.resolve(data.location)];
				Mod = require(data.location);
			} catch (e) {
				console.log(e);
				this.logger.warn('Loader', `Unable to load module ${data.type} '${data.location}' (Require failed: ${e})`);
				continue;
			}

			// Check if we can instantiate it
			if (!Mod || typeof Mod !== 'function') {
				this.logger.warn('Loader', `Unable to load module ${data.type} '${data.location}' (Not a function on module.exports)`);
				continue;
			}

			if (moduleType === 'core') {
				// instantiate core module
				data.mod = new Mod(this.base);

				// check if core module wants to override it's name
				if (data.mod.hasOwnProperty('name')) data.name = data.mod.name;

				// core module dependencies
				data.dependencies = data.mod.dependencies || null;

				// is this core modules required to load succesfully before contining?
				data.required = data.mod.required !== undefined ? data.mod.required : null;
			} else {
				// Non-core modules should not be instantiated here, simply passing constructor
				data.mod = Mod;
			}

			// Push to array
			loaded.push(data);
		}

		return loaded;
	}
}

module.exports = ModuleLoader;
