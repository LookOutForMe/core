'use strict';

const path = require('path');

const { asyncAwaitForEach } = require('../Util/Async');
const FileSystem = require('../Util/FileSystem');
const { createID } = require('../Generator');

const log = require('@ayana/logger').get('ModuleLoader', __dirname);

class ModuleLoader {
	constructor(opts) {
		opts = Object.assign({}, {
			base: {},
		}, opts);

		this.base = opts.base;

		this.primary = new Map();
		this.secondary = new Map();
	}

	/**
	 * Alias for getPrimary
	 * @param {string} name - Primary Module name
	 * @returns {Object} - Instantiated module
	 */
	get(name) {
		return this.getPrimary(name);
	}

	/**
	 * Access a primary module
	 * @param {string} name - Primary module name
	 * @returns {Object} - Instantiated module
	 */
	getPrimary(name) {
		if (!this.primary.has(name)) throw new Error(`PrimaryModule '${name}' does not exist!`);
		return this.primary.get(name).instance;
	}

	/**
	 * Access a secondary module, this is an anti-pattern!
	 * If you need to access a secondary module. You should probably just make it a primary module.
	 * @param {string} name - Secondary module name
	 * @returns {Object} - Instantiated module
	 */
	getSecondary(name) {
		if (!this.secondary.has(name)) throw new Error(`SecondaryModule '${name}' does not exist!`);
		return this.secondary.get(name).instance;
	}

	/**
	 * Instantiates and registers all primary and secondary modules.
	 * @param {Object} opts - Module Options
	 * @param {string} opts.primary - Primary modules location
	 * @param {string} opts.secondary - Secondary modules location
	 */
	async init(opts) {
		opts = Object.assign({}, {
			primary: null,
			secondary: null,
		}, opts);

		if (!opts.primary) throw new Error(`'opts.primary' is required.`);

		// load modules
		await this.loadPrimaryModules(opts.primary);
		if (opts.secondary) await this.loadSecondaryModules(opts.secondary);
	}

	/**
	 * Registers module of provided type, also calls onMount on module
	 * @param {string} type - Type of module to register: primary, secondary.
	 * @param {Object} data - Module data
	 * @returns {boolean}
	 */
	async registerModule(type, data) {
		if (!data.name || !data.instance || !data.location) throw new Error(`Cannot register ${type} module. Missing required data`);

		if (data.instance.onMount && typeof data.instance.onMount === 'function') {
			try {
				await data.instance.onMount();
			} catch (e) {
				log.error(`onMount: ${data.name}(${data.location}). ${e}`);

				if (type === 'primary' && data.required) throw new Error(`ModuleLoader: ${data.name}(${data.location}) stated it is a required module. ${e}`);
			}
		}

		if (type === 'primary') {
			this.primary.set(data.name, data);
		} else {
			this.secondary.set(data.name, data);
		}

		log.debug(`Registered ${type} module ${data.name}(${data.location})`);
		return true;
	}

	/**
	 * Instantiates and registers primary modules
	 * @param {string} directory - Directory to load primary modules from
	 */
	async loadPrimaryModules(directory) {
		log.info(`Loading primary modules...`);
		const modules = await this.loadModules(directory);

		let count = 0;
		let waiting = [];
		const determineMissing = dependencies => dependencies.reduce((a, dependency) => {
			if (!this.primary.has(dependency)) a.push(dependency);

			return a;
		}, []);

		const handleWaiting = async() => {
			if (waiting.length === 0) return;

			let registerdOne = false;
			const newWaiting = [];
			await asyncAwaitForEach(waiting, async data => {
				const missing = determineMissing(data.dependencies);

				if (missing.length === 0) {
					await this.registerModule('primary', data);
					count++;
					registerdOne = true;
				} else newWaiting.push(data);
			});

			waiting = newWaiting;
			if (registerdOne) return handleWaiting();
		};

		await asyncAwaitForEach(modules, async({ name, module, location }) => {
			const instance = new module(this.base);

			// primary modules are allowed to specify their own name
			if (instance.hasOwnProperty(name)) name = instance.name;

			// primary module propetries
			const dependencies = instance.dependencies || null;
			const required = instance.required || null;

			// primary module requires no dependencies, load right away
			const data = {
				name,
				instance,
				location,
				dependencies,
				required,
			};

			if (!data.dependencies || data.dependencies.length === 0) {
				await this.registerModule('primary', data);
				count++;
				return handleWaiting();
			} else {
				waiting.push(data);
				log.debug(`Delaying registration of ${data.name}(${data.location}). Dependencies: ${data.dependencies.join(', ')}`);
				return handleWaiting();
			}
		});

		if (waiting.length > 0) {
			waiting.forEach(({ name, location, dependencies, required }) => {
				const missing = determineMissing(dependencies);
				log.error(`Unable to load primary module ${name}(${location}). Missing depnedencies: ${missing.join(', ')}.`);
				if (required) throw new Error(`ModuleLoader: ${name}(${location}) stated it is a required module.`);
			});
		}

		await asyncAwaitForEach(Array.from(this.primary.values()), async data => {
			if (data.instance.onEnable && typeof data.instance.onEnable === 'function') {
				try {
					await data.instance.onEnable();
				} catch (e) {
					log.error(`onEnable: ${data.name}(${data.location}). ${e}`);
				}
			}
		});

		log.info(`Loaded and Mounted ${count} primary modules.`);
	}

	/**
	 * Instantiates and registers secondary modules
	 * @param {string} directory - Directory to load secondary modules from
	 */
	async loadSecondaryModules(directory) {
		const modules = await this.loadModules(directory);

		const api = Array.from(this.primary.entries()).reduce((a, [name, { instance }]) => {
			a[name] = instance;

			return a;
		});

		let count = 0;
		await asyncAwaitForEach(modules, async({ name, module, location }) => {
			const instance = new module(this.base, api);

			// prevent duplicate named secondary modules
			if (this.secondary.has(name)) name = `${name}-${createID(4)}`;

			const data = {
				name,
				instance,
				location,
			};

			await this.registerModule('secondary', data);

			if (data.instance.onEnable && typeof data.instance.onEnable === 'function') {
				try {
					await data.instance.onEnable();
				} catch (e) {
					log.error(`onEnable: ${data.name}(${data.location}). ${e}`);
				}
			}

			count++;
		});

		log.info(`Loaded and Mounted ${count} secondary modules.`);
	}

	/**
	 * Loads module files and directories from specified location
	 * @param {string} location - Filesystem location to load modules from
	 *
	 * @typedef {Object} Module
	 * @property {string} name - Parsed name of module
	 * @property {function|Object} module - Object or function to be instantiated
	 * @returns {[Module]}
	 */
	async loadModules(location) {
		if (!await FileSystem.checkExists(location)) throw new Error(`Loader: Module Location '${location}' does not exist!`);

		let entries = null;
		try {
			entries = await FileSystem.directoryListing(location);
		} catch (e) {
			throw new Error(`Error fetching listing: ${e}`);
		}

		const modules = [];
		await asyncAwaitForEach(entries, async item => {
			let name = null;

			if (await FileSystem.isDirectory(item)) {
				// is directory
				if (!await FileSystem.checkExists(path.resolve(item, 'index.js'))) {
					log.warn(`Unable to load module '${item}'. No 'index.js' found.`);
					return;
				}

				name = item.split('/').slice(-1)[0];
			} else {
				// is file
				if (!/.*\.js$/i.test(item)) {
					log.warn(`Unable to load module '${item}'. Not a javascript file.`);
					return;
				}

				name = item.split('/').slice(-1)[0].slice(0, -3);
			}

			log.debug(`Loading module '${item}'`);

			let module = null;
			try {
				delete require.cache[require.resolve(item)];
				module = require(item);
			} catch (e) {
				log.warn(`Unable to load module '${item}'. Failed to require: ${e}`);
				return;
			}

			if (!module || typeof module !== 'function') {
				log.warn(`Unable to load module '${item}'. Not a function on module.exports`);
				return;
			}

			modules.push({ name, module, location: item });
		});

		return modules;
	}
}

module.exports = ModuleLoader;
