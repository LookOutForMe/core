'use strict';

const crypto = require('crypto');

exports.createID = (len=24) => {
	return crypto.randomBytes(len).toString('base64').replace(/[^a-z0-9]/gi, '').slice(0, len);
};