'use strict';

module.exports = (emitter, events, fn, opts) => {
	return new Promise((resolve, reject) => {
		if (!emitter || !events || !fn) throw new Error(`'emitter', 'event(s)', 'fn' arguments are all required.`);
		if (typeof fn !== 'function') throw new Error(`'fn' argument is not a valid function.`);
		
		// set defaults
		opts = Object.assign({}, {
			timeout: 3 * 1000,
			once: false,
		}, opts);

		const listeners = [];

		const handleCleanup = () => {
			clearTimeout(timer);
			listeners.forEach(listener => {
				emitter.removeListener(listener.event, listener.listener);
			});
		};

		const handleEvent = async (event, ...args) => {
			const result = await fn(event, ...args);

			if (result === true) {
				handleCleanup();
				resolve();
			}
		};

		const handleTimeout = () => {
			handleCleanup();
			return reject(new Error(`Exceeded '${opts.timeout}'ms timeout waiting for events: ${events.join(', ')}`));
		};

		// has to be sync
		events.forEach(event => {
			const asyncRemap = (...args) => {
				handleEvent(event, ...args).catch(e => {
					handleCleanup();
					reject(new Error(`Error while processing event: ${e}`));
				});
			};

			emitter[opts.once ? 'once' : 'on'](event, asyncRemap);
			listeners.push({ event, listener: asyncRemap });
		});

		const timer = setTimeout(handleTimeout, opts.timeout);
	});
};