'use strict';

/*
 * Load env variables
 */
class Config {
	constructor(vars, required) {
		this.required = required || [];
		this.vars = vars.concat(this.required);
	}

	load() {
		const missing = [];
		for (let req of this.required) {
			if (Object.keys(process.env).indexOf(req) < 0) missing.push(req);
		}

		if (missing.length > 0) throw new Error(`The following config keys are missing: ${missing.join(', ')}`);

		for (let item of this.vars) {
			if (process.env[item]) this[item] = process.env[item];
		}
	}
}

module.exports = Config;
