'use strict';

const RedisClient = require('redis-fast-driver');

const url = require('url');
const { EventEmitter } = require('events');

class Redis extends EventEmitter {
	constructor(dsn, opts) {
		super();

		opts = Object.assign({}, {
			logger: console.log,
			prefix: null,
			prefixPolicy: false,
		}, opts);

		this.prefix = opts.prefix;
		this.prefixPolicy = opts.prefixPolicy;

		const parsed = url.parse(dsn, true);
		if (!parsed.hostname || !parsed.port) throw new Error('Error parsing Redis DSN');

		this.host = parsed.hostname;
		this.port = parsed.port;
		
		this.auth = null;

		this.db = null;

		if (parsed.query && parsed.query.db) this.db = parsed.query.db;
		if (parsed.query && parsed.query.auth) this.auth = parsed.query.auth;

		this.cli = null;
	}

	async init() {
		this.cli = new RedisClient({
			host: this.host,
			port: this.port,
			maxRetries: -1,
			auth: this.auth,
			db: this.db,
			autoConnect: true,
		});

		//happen only once
		this.cli.on('ready', () =>{
			this.emit('ready');
		});

		//happen each time when reconnected
		this.cli.on('connect', () => {
			this.emit('connect');
		});

		this.cli.on('disconnect', () => {
			this.emit('disconnect');
		});

		this.cli.on('reconnecting', (num) => {
			this.emit('reconnecting', num);
		});

		this.cli.on('error', (e) => {
			this.emit('error', e);
		});

		// called on an explicit end, or exhausted reconnections
		this.cli.on('end', () => {
			this.emit('end');
		});
	}

	parseOpts(opts) {
		return Object.assign({}, {
			prefix: this.prefixPolicy,
			json: false,
		}, opts);
	}

	async rawCall(raw) {
		return this.cli.rawCallAsync(raw);
	}

	async get(k, opts) {
		opts = this.parseOpts(opts);
		if (opts.prefix && this.prefix) k = `${this.prefix}${k}`;
		
		const resp = await this.rawCall(['get', k]);

		if (opts.json) return JSON.parse(resp);

		return resp;
	}

	async set(k, v, opts) {
		opts = this.parseOpts(opts);
		if (opts.prefix && this.prefix) k = `${this.prefix}${k}`;

		if (opts.json && typeof v !== 'string') v = JSON.stringify(v);

		return this.rawCall(['set', k, v]);
	}

	async setex(k, s, v, opts) {
		opts = this.parseOpts(opts);
		if (opts.prefix && this.prefix) k = `${this.prefix}${k}`;

		if (opts.json && typeof v !== 'string') v = JSON.stringify(v);

		return this.rawCall(['setex', k, s, v]);
	}

	async incr(k, opts) {
		opts = this.parseOpts(opts);
		if (opts.prefix && this.prefix) k = `${this.prefix}${k}`;

		return this.rawCall(['incr', k]);
	}

	async llen(k, opts) {
		opts = this.parseOpts(opts);
		if (opts.prefix && this.prefix) k = `${this.prefix}${k}`;

		return this.rawCall(['llen', k]);
	}

	async lpop(k, opts) {
		opts = this.parseOpts(opts);
		if (opts.prefix && this.prefix) k = `${this.prefix}${k}`;

		return this.rawCall(['lpop', k]);
	}

	async lpush(k, vals, opts) {
		opts = this.parseOpts(opts);
		if (opts.prefix && this.prefix) k = `${this.prefix}${k}`;

		return this.rawCall(['lpush', k].concat(vals));
	}

	async lrange(k, start, stop, opts) {
		opts = this.parseOpts(opts);
		if (opts.prefix && this.prefix) k = `${this.prefix}${k}`;

		return this.rawCall(['lrange', k, start, stop]);
	}

	async ttl(k, opts) {
		opts = this.parseOpts(opts);
		if (opts.prefix && this.prefix) k = `${this.prefix}${k}`;

		return this.rawCall(['ttl', k]);
	}

	async expire(k, s, opts) {
		opts = this.parseOpts(opts);
		if (opts.prefix && this.prefix) k = `${this.prefix}${k}`;

		return this.rawCall(['expire', k, s]);
	}
}

module.exports = Redis;
