'use strict';

const { InternalError } = require('../Errors');

const Raven = require('raven');

const { Util } = require('../Util');

const log = require('@ayana/logger').get('Sentry', __dirname);

class Sentry {
	constructor(dsn, extra) {
		this.dsn = dsn;
		this.enabled = false;

		if (!this.dsn) return;

		Raven.config(this.dsn, extra);

		Raven.install((e, se, eID) => {
			try {
				if (se) log.error('Sentry', `Sentry sending error (${eID}): ${se}`);
				if (e) log.error('Sentry', `Sentry error (${eID}): ${e}`);
			} catch (err) {
				log.fatal('Sentry', `Error in fatal error function: ${err}`);
			}
		});

		Raven.disableConsoleAlerts();

		this.enabled = true;
	}

	context(ctxOrFunc, func) {
		if (!this.enabled) return;
		return Raven.context(ctxOrFunc, func);
	}

	captureBreadcrumb(breadcrumb) {
		if (!this.enabled) return;
		return Raven.captureBreadcrumb(breadcrumb);
	}

	captureException(err, extra, from) {
		if (!this.enabled) return;

		extra = extra || {};
		extra.reportedFrom = from || 'Unknown';

		return Raven.captureException(err, { extra });
	}

	captureEnhancedException(err, extra, from) {
		if (!this.enabled) return;

		extra = extra || {};
		extra.original = err;

		try {
			extra.stringify = Util.stringifyCircular(err);
		} catch (e) {
			extra.stringify = 'Failed to stringify';
		}

		let error;
		if (err.message) error = err;
		else if (err.substring) error = new InternalError(err);
		else if (extra.stringify !== 'Failed to stringify') error = new InternalError(extra.stringify);
		else error = new InternalError('Unknown error, see additional data for more information');

		this.captureException(error, extra, from);
	}
}

module.exports = Sentry;
