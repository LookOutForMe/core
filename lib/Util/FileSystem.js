'use strict';

const fs = require('fs');
const path = require('path');

exports.checkExists = location => new Promise(resolve => {
	fs.access(location, fs.constants.F_OK, e => {
		if (e) return resolve(false);

		resolve(true);
	});
});


exports.isDirectory = location => new Promise(resolve => {
	fs.stat(location, (e, stat) => {
		if (e) throw e;

		resolve(stat.isDirectory());
	});
});

exports.directoryListing = location => new Promise(resolve => {
	fs.readdir(location, (e, files) => {
		if (e) throw e;

		resolve(files.map(file => path.resolve(location, file)));
	});
});
