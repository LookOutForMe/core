'use strict';

module.exports.stringifyCircular = (obj, space) => {
	let cache = [];
	const stringify = JSON.stringify(obj, (key, value) => {
		if (typeof value === 'object' && value !== null) {
			if (cache.indexOf(value) !== -1) return '[Circular]';
			cache.push(value);
		}
		return value;
	}, space);
	cache = null;
	return stringify;
};
