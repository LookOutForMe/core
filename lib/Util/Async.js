'use strict';

/**
 * Runs an asnychronous iteration over an object or array.
 * Each iteration is run on a different node tick.
 * The function can throw an error with the message "resolve" to stop the iteration.
 *
 * @param {O} o The object or array to be iterated over
 * @param {function(any,string,O,number)} fn - The async function to be called on every interation
 * @returns {Promise<void>} A promise that resolves when the operation is done. Will reject when fn throws an unexpected error.
 * @template O
 */
exports.asyncForEach = (o, fn) => new Promise((resolve, reject) => {
	let keys = Object.keys(o), offset = 0;
	if (keys.length < 1) return resolve();

	(function next() {
		try {
			fn(o[keys[offset]], keys[offset], o, offset);
		} catch (e) {
			if (e.message === 'resolve') return resolve();
			return reject(e);
		}

		if (++offset < keys.length) global.setImmediate(next);
		else resolve();
	}());
});

/**
 * Runs an asnychronous iteration over an object or array.
 * Expects fn to be a Promise and waits until that Promise resolves before continuing.
 * The function can throw an error with the message "resolve" to stop the iteration.
 *
 * @param {O} o The object or array to be iterated over
 * @param {function(any,string,O,number)} fn - The async function to be called on every interation
 * @returns {Promise<void>} A promise that resolves when the operation is done. Will reject when fn throws an unexpected error.
 * @template O
 */
exports.asyncAwaitForEach = (o, fn) => {
	return new Promise((resolve, reject) => {
		let keys = Object.keys(o), offset = 0;
		if (keys.length < 1) return resolve();

		(function next() {
			fn(o[keys[offset]], keys[offset], o, offset)
			.then(() => {
				if (++offset < keys.length) global.setImmediate(next);
				else resolve();
			})
			.catch(e => {
				if (e.message === 'resolve') return resolve();
				return reject(e);
			});
		}());
	});
};

/**
 * Runs an asnychronous iteration over a Map
 * Each iteration is run on a different node tick.
 * The function can throw an error with the message "resolve" to stop the iteration.
 *
 * @param {Map<K,V>} map The map to be iterated over
 * @param {function(V,K,Map<K,V>)} fn - The function to be called on every interation
 * @returns {Promise<void>} A promise that resolves when the operation is done. Will reject when fn throws an unexpected error.
 * @template K,V
 */
exports.asyncMapForEach = (map, fn) => {
	let iterator = map[Symbol.iterator]();
	return new Promise((resolve, reject) => {
		(function next() {
			let value = iterator.next().value;
			if (value == undefined) return resolve();
			if (!(value instanceof Array) || !value.length === 2) reject(new Error('No valid Map object given for iteration'));

			try {
				fn(value[1], value[0], map);
			} catch (e) {
				if (e.message === 'resolve') return resolve();
				return reject(e);
			}

			global.setImmediate(next);
		}());
	});
};

/**
 * Runs an asnychronous iteration over a Map
 * Expects fn to be a Promise and waits until that Promise resolves before continuing.
 * The function can throw an error with the message "resolve" to stop the iteration.
 *
 * @param {Map<K,V>} map The map to be iterated over
 * @param {function(V,K,Map<K,V>)} fn - The function to be called on every interation
 * @returns {Promise<void>} A promise that resolves when the operation is done. Will reject when fn throws an unexpected error.
 * @template K,V
 */
exports.asyncMapAwaitForEach = (map, fn) => {
	let iterator = map[Symbol.iterator]();
	return new Promise((resolve, reject) => {
		(function next() {
			let value = iterator.next().value;
			if (value == undefined) return resolve();
			if (!(value instanceof Array) || !value.length === 2) reject(new Error('No valid Map object given for iteration'));

			fn(map, value[1], value[0], map)
			.then(() => {
				global.setImmediate(next);
			})
			.catch(e => {
				if (e.message === 'resolve') return resolve();
				return reject(e);
			});
		}());
	});
};

/**
 * Filters values out of a map asynchronously
 *
 * @param {Map<K,V>} map The map to filter from
 * @param {any|function(V,K)} objOrFn An object the key will be compared with or a function that returns a boolean
 * @returns {Promise<Array<V>>} The results of the filter
 * @template K,V
 */
exports.asyncMapFilter = async(map, objOrFn) => {
	let results = [];
	if (typeof condition !== 'function') objOrFn = (k) => k === objOrFn;
	await exports.asyncMapForEach(map, (v, k) => {
		if (objOrFn(v, k)) results.push(v);
	});
	return results;
};

/**
 * Finds a value of a map asynchronously
 *
 * @param {Map<K,V>} map The map to search in
 * @param {any|function(V,K)} objOrFn An object the key will be compared with or a function that returns a boolean
 * @returns {Promise<V>} The result of the find or null if nothing was found
 * @template K,V
 */
exports.asyncMapFind = async(map, objOrFn) => {
	let item = null;
	if (typeof condition !== 'function') objOrFn = (k) => k === objOrFn;
	await exports.asyncMapForEach(map, (v, k) => {
		if (objOrFn(v, k)) {
			item = v;
			throw new Error('resolve');
		}
	});
	return item;
};

/**
 * Runs a setTimeout which will resolve after the specified ms value
 *
 * @param {numer} ms The milliseconds to wait until resolving
 * @returns {Promise<void>} A promise
 */
exports.asyncTimeout = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

/**
 * Runs a setImmediate which will resolve in the next process tick
 *
 * @returns {Promise<void>} A promise
 */
exports.asyncImmediate = () => new Promise((resolve) => setImmediate(resolve));

/**
 * Promisifies a function
 * @see NodeJS util.promisify()
 */
exports.promisify = require('util').promisify;
