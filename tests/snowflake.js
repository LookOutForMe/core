const Flurry = require('../index').Flurry;

const flurry = new Flurry();

const flakes = [];

for(let i = 0; i < 99999; i++) flakes.push(flurry.next());

console.log(flakes.join('\n'));