'use strict';

const { RedisClient, Generator } = require('../lib');

let redis = new RedisClient('redis://127.0.0.1:6379');

redis.on('ready', () => {
	console.log('Successfuly connected. Doing test then exiting...');
	redis.setex('_ayanacore.redistest', 5, Generator.createID());
	redis.get('_ayanacore.redistest').then(id => {
		console.log('from redis:', id);
		// process.exit(0);
	});
});

redis.on('connect', () => {
	console.log('reconnected');
});

redis.on('reconnecting', retry => {
	console.log('reconnecting...', retry);
});

redis.on('error' , e => {
	console.log(e);
})

redis.on('end', () => {
 	console.log('end');
})


redis.init();

const a = () => setTimeout(() => {
 a();
}, 5000);

a();
