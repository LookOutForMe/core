'use strict';

const Queue = require('../index').Queue;

const q = new Queue();

// enqueue to position
q.enqueue({ ex: 'a' }, 99);
console.log(q.items);

q.enqueue({ ex: 'b' }, 0);
console.log(q.items);

q.enqueue({ ex: 'c' }, 2);
console.log(q.items);

q.enqueue({ ex: 'd' }, 1);
console.log(q.items);

// move range
q.moveRange(2, 3, 1);
console.log(q.items);

q.moveRange(0, 0, 2);
console.log(q.items);

// delete indexes
q.deleteIndexes([ 0, 3 ]);
console.log(q.items);