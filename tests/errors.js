'use strict';

const { FriendlyError, InternalError, LocalizedError } = require('../').Errors;

const test = new InternalError('test.error', 'A user friendly message');

if (InternalError.isInternalError(test)) console.log(test.message);
else console.log('test wasn\'t a internal error');
