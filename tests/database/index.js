'use strict';

const path = require('path');

const { Database } = require('../../lib');
const DB = new Database();

DB.init('mongodb://127.0.0.1/test').then(async() => {
	const { User } = await DB.loadModels(path.resolve(__dirname, 'models'));

	const user = new User({ userID: '145162973910925312', username: 'HcgRandon', roles: ['owner', 'admin'] });
	user.save();
});
