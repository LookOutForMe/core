'use strict';

module.exports = {
	name: 'User',
	schema: {
		userID: { type: String, index: true, unique: true },
		username: String,
		roles: Array,
	},
};
