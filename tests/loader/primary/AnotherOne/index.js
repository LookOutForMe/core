'use strict';

class AndAnotherOne {
	constructor(base) {
		this.base = base;

		this.name = 'NameOverride';
		this.dependencies = ['Example'];

		this.required = true;
	}
}

module.exports = AndAnotherOne;
