'use strict';

class Example {
	constructor() {
		this.required = true;
	}

	async onMount() {
		console.log('awoo');
	}

	async boo() {
		console.log('Get spooped!!!!11!');
	}
}

module.exports = Example;
