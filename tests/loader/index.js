'use strict';

const path = require('path');

const { Logger, ModuleLoader } = require('../../');

const base = {
	cfg: {
		blah: 'de dah',
	},
};

const logger = new Logger();
logger.mode = 'debug';

const loader = new ModuleLoader({
	base,
	logger,
});

loader.init({
	primary: path.resolve(__dirname, 'primary'),
	secondary: path.resolve(__dirname, 'secondary'),
})
.then(() => {
	console.log('Primary Modules:', Array.from(loader.primary.keys()).join(', '));
	console.log('Secondary Modules:', Array.from(loader.secondary.keys()).join(', '));

	loader.get('Example').boo();
});
