const { EmitterTools } = require('../lib').Util;

const { EventEmitter } = require('events');

const emitter = new EventEmitter();

EmitterTools.timedListener(emitter, ['test1', 'test2'], async (event, ...args) => {
	console.log(event, ...args);
	if (event === 'test2') return true;
}).catch(e => {
	console.log(e);
});

emitter.emit('test1', 'hello');
emitter.emit('test2', 'world');
