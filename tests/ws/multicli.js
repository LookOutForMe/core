'use strict';

const { MultiGatewayClient } = require('../../index').Net;

const multi = new MultiGatewayClient({ type: 'shard' });

try {
 multi.connect({
  dsns: [
   'ayanagw://127.0.0.1:27532',
   'ayanagw://192.168.1.2:27532',
  ],
 });
} catch (e) {
 console.log(e);
}

multi.on('open', e => {
 console.log('Attempt to connect to: ', e.host, e.port);
});

multi.on('connected', e => {
 console.log(e);
});

multi.on('error', e => {
 console.log(e);
});

multi.on('rawWS', e => {
 console.log(e);
});

multi.on('dispatch', ({ pkt }) => {
 console.log(pkt);
 switch (pkt.t) {
  case 'reshard':
   console.log('Reshard event!!111!1!!!11!');

   multi.multiSendPKT({
    op: 0,
    t: 'shardsinfo',
    d: null,
    dst: {
     t: 'shard',
     gw: true,
    },
   });
   break;
 }
});

// const cli = new GatewayClient('shard', 27532, '127.0.0.1');
// cli.connect();

// cli.on('open', d => {
//  console.log(`opened connection on ${d.ip}:${d.port}`);
// });



// cli.on('error', e => {
//  console.log(e);
// });

// cli.on('rawWS', d => {
//  console.log(JSON.stringify(d));
// });